import React, { useEffect, useState } from 'react'
import Aircard from './Aircard';

const New = () => {
    const [latitude, setlatitude] = useState({});
    const [longitude, setlongitude] = useState({});
    const [data, setData] = useState([]);

    const getLocation = () => {
        navigator.geolocation.getCurrentPosition((position) => {
            setlatitude({ latitude: position.coords.latitude });
            setlongitude({ longitude: position.coords.longitude });
        })
    };

    const getData = async () => {
        const responseData = await (await fetch(`http://api.weatherapi.com/v1/forecast.json?key=294af596dfd94f61993103141222707&q=${latitude.latitude},${longitude.longitude}&days=5`)).json();
        setData(responseData);
    }

    useEffect(() => {
        getLocation();
        getData();
    }, [latitude, longitude]);

    console.log(data);

    return (
        <div>
            {typeof data.location != "undefined" ?
                <Aircard data={data} /> :
                <div className='text-white-50'>Loading...</div>
            }
        </div>
    )
}

export default New