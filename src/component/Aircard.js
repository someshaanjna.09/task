import React from 'react'

const Aircard = ({ data }) => {
    return (
        <>
            <div class="card bg-black bg-opacity-25 border-0" style={{ width: "20rem" }}>
                <div class="card-body">
                    <div className='d-flex-column'>
                        <h5 class="text-white display-1">{data.location.name}</h5>
                        <h6 class="text-white-50">Day: {data.forecast.forecastday[0].date}</h6>
                        <h6 class="text-white-50">Temperature: {data.current.temp_c}<sup>&deg;C</sup></h6>
                        <h6 class="text-white-50">Humidity: {data.current.humidity}</h6>
                        <h6 class="text-white-50">Sunrise: {data.forecast.forecastday[0].astro.sunrise}</h6>
                        <h6 class="text-white-50">Sunset: {data.forecast.forecastday[0].astro.sunset}</h6>
                    </div>
                </div>
            </div>
        </>
    )
}

export default Aircard